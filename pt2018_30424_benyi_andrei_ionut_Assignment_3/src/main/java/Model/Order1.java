package Model;
/**
 * Class for the Order1 table in the DB
 * @author Andrei Ionut Benyi
 *
 */
public class Order1 {
	private int id;
	private String date;
	private int prID;
	
	public Order1(int id, String dat,int prid) {
		super();
		this.id=id;
		this.date=dat;
		this.prID=prid;
	}
	
	public Order1(String dat,int prid) {
		super();
		this.date=dat;
		this.prID=prid;
	}
	
	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id=id;
	}
	
	public int getPrID() {
		return this.prID;
	}
	public void setPrID(int id) {
		this.prID=id;
	}
	
	public String getDate() {
		return this.date;
	}
	public void setDate(String s) {
		this.date=s;
	}
}
