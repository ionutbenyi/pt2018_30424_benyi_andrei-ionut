package Model;
/**
 * Class for the Product table in the DB
 * @author Andrei Ionut Benyi
 *
 */
public class Product {
	private int id;
	private String brand;
	private String model;
	private int price;
	private int quantity;
	
	public Product(int i, String b, String m, int p, int q) {
		this.id=i;
		this.brand=b;
		this.model=m;
		this.price=p;
		this.quantity=q;
	}
	
	public Product(String b, String m, int p,int q) {
		this.brand=b;
		this.model=m;
		this.price=p;
		this.quantity=q;
	}
	
	public void setId(int i) {
		this.id=i;
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setBrand(String s) {
		this.brand=s;
	}
	
	public String getBrand() {
		return this.brand;
	}
	
	public void setModel(String s) {
		this.model=s;
	}
	
	public String getModel() {
		return this.model;
	}
	
	public void setPrice(int i) {
		this.price=i;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public void setQuantity(int i) {
		this.quantity=i;
	}
	
	public int getQuantity() {
		return this.quantity;
	}
	
}
