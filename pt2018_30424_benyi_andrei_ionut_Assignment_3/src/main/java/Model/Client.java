package Model;
/**
 * Class for the Client table in the DB
 * @author Andrei Ionut Benyi
 *
 */
public class Client {
	private int id;
	private String name;
	private String phone;
	private String address;
	private int age;
	private String email;
	
	public Client(int id, String name, String phone, String address, int age, String email) {
		super();
		this.id=id;
		this.name=name;
		this.phone=phone;
		this.address=address;
		this.age=age;
		this.email=email;
	}
	
	public Client(String name, String phone, String address, int age, String email) {
		super();
		this.name=name;
		this.phone=phone;
		this.address=address;
		this.age=age;
		this.email=email;
	}
	
	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id=id;
	}
	
	public String getName() {
		return this.name;
	}
	public void setName(String s) {
		this.name=s;
	}
	
	public String getAddress() {
		return this.address;
	}
	public void setAddress(String add) {
		this.address=add;
	}
	
	public String getPhone() {
		return this.phone;
	}
	public void setPhone(String p) {
		this.phone=p;
	}
	
	public int getAge() {
		return this.age;
	}
	public void setAge(int age) {
		this.age=age;
	}
	
	public String getEmail() {
		return this.email;
	}
	public void setEmail(String em) {
		this.email=em;
	}
}
