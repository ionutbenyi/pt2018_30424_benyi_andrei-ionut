package BLL;
import java.util.*;
import DAO.ProductDAO;
import Model.Product;

public class ProductBLL {
	private ArrayList<Validator<Product>> v;
	
	public ProductBLL() {
		v=new ArrayList<Validator<Product>>();
		v.add(new PriceV());
	}
	
	public Product findProductByID(int i) {
		Product p=ProductDAO.findUsingID(i);
		if(p==null) {
			throw new NoSuchElementException("Not found Product with id "+i);
		}
		return p;
	}
	
	public Product insertProduct(Product p) {
		for(Validator<Product> vp: v) {
			vp.validate(p);
		}
		return ProductDAO.insert(p);
	}
}
