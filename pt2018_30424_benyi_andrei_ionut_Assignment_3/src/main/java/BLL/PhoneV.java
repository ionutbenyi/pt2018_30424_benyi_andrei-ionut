package BLL;

import Model.Client;
public class PhoneV implements Validator<Client>{
	public void validate(Client c) {
		if(c.getPhone().length()>10) {
			throw new IllegalArgumentException("Not a valid phone nr");
		}
	}
}
