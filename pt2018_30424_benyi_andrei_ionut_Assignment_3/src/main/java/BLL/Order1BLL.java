package BLL;
import java.util.*;
import DAO.Order1DAO;
import Model.Order1;

public class Order1BLL {
	private ArrayList<Validator<Order1>> v;
	
	public Order1BLL() {
		v=new ArrayList<Validator<Order1>>();
		v.add(new DateV());
	}
	
	public Order1 findClientById(int i) {
		Order1 o=Order1DAO.findUsingID(i);
		if(o==null) {
			throw new NoSuchElementException("no Order with id "+i);
		}
		return o;
	}
	
	public Order1 insertClient(Order1 o) {
		for(Validator<Order1> vo: v) {
			vo.validate(o);
		}
		
		return Order1DAO.insert(o);
	}
}
