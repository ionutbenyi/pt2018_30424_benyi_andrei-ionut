package Presentation;

import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.sql.*;
import com.mysql.cj.core.result.Field;

import ConnectionDB.ConnectionFactory;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.*;
import Model.Client;
import DAO.ClientDAO;

public class Controller3 {
	
	private View3 v;
	public ArrayList<Client> ca;
	
	public Controller3(View3 v3) {
		ca=new ArrayList<Client>();
		this.v=v3;
		
		v.addAction(new AddListener());
		v.updateAction(new UpdateListener());
	}
	
	class AddListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(v.clAddressTF.getText().isEmpty() || v.clAgeTF.getText().isEmpty() || v.clEmailTF.getText().isEmpty() || v.clNameTF.getText().isEmpty() || v.clPhoneTF.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Error. Check parameters");
			}
			else {
				String clAddress=v.clAddressTF.getText();
				int clAge=Integer.parseInt(v.clAgeTF.getText());
				String clEmail=v.clEmailTF.getText();
				String clName=v.clNameTF.getText();
				String clPhone=v.clPhoneTF.getText();
				Client c=new Client(clName,clPhone,clAddress,clAge,clEmail);
				c=ClientDAO.insert(c);
				
				v.mainFr.setVisible(false);
				v=new View3();
			}
		}
	}
	
	class UpdateListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(v.clIDTF.getText().isEmpty() || v.clNameTF.getText().isEmpty() || v.clPhoneTF.getText().isEmpty() || v.clAddressTF.getText().isEmpty() || v.clAgeTF.getText().isEmpty() || v.clEmailTF.getText().isEmpty()) {
				//JOptionPane.showMessageDialog(null, "Error. Check parameters");
			}
			else {
				int clID=Integer.parseInt(v.clIDTF.getText());
				String clAddress=v.clAddressTF.getText();
				int clAge=Integer.parseInt(v.clAgeTF.getText());
				String clEmail=v.clEmailTF.getText();
				String clName=v.clNameTF.getText();
				String clPhone=v.clPhoneTF.getText();
				Client c=new Client(clName,clPhone,clAddress,clAge,clEmail);
				c=ClientDAO.update(c,clID);
				
				v.mainFr.setVisible(false);
				v=new View3();
			}
		}
	}
}
