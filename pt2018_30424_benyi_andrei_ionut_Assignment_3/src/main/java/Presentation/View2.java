package Presentation;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import ConnectionDB.ConnectionFactory;
import Model.Product;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.*;

public class View2 {
	
	
	public JFrame mainFr=new JFrame();
	private JPanel mainPanel=new JPanel();
	
	public JTable tProducts=new JTable();
	public JScrollPane a=new JScrollPane(tProducts);

	private JLabel prBrand;
	public JTextField prBrandTF;
	private JLabel prID;
	public JTextField prIDTF;
	private JLabel prModel;
	public JTextField prModelTF;
	private JLabel prPrice;
	public JTextField prPriceTF;
	private JLabel prQuantity;
	public JTextField prQuantityTF;
	
	private JButton addPr;
	private JButton updPr;
	
	public JPanel cp1;
	public JPanel cp2;
	
	//-----------------------------------------------------
	public ArrayList<Product> products(){
		ArrayList<Product> ap=new ArrayList<Product>();
		
		try {
			Connection dbConnection = ConnectionFactory.getConnection();
			String inst="SELECT * FROM Product";
			Statement stat=dbConnection.createStatement();
			ResultSet resSet=stat.executeQuery(inst);
			
			Product p;
			while(resSet.next()) {
				p=new Product(resSet.getInt("id"), resSet.getString("brand"), resSet.getString("model"), resSet.getInt("price"), resSet.getInt("quantity"));
				ap.add(p);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return ap;
	}
	
	public static String[][] createDataM(ArrayList p ){
		String [][]res=new String[p.size()][p.get(0).getClass().getDeclaredFields().length];
		
		int i=0,j=0;
		for(Object pr: p) {
			j=0;
			for(java.lang.reflect.Field f: pr.getClass().getDeclaredFields()) {
				f.setAccessible(true);
				try {
					res[i][j]=f.get(pr).toString();
				}catch(IllegalArgumentException e) {
					e.printStackTrace();
				}catch(IllegalAccessException e) {
					e.printStackTrace();
				}
				j++;
			}
			i++;
		}
		
		return res;
	}
	//-----------------------------------------------------
	
	public View2() {
		mainFr.setSize(1000,300);
		mainFr.setLocationRelativeTo(null);
		mainFr.setLayout(new FlowLayout());
		prID=new JLabel("ID:");
		prIDTF=new JTextField("");
		prBrand=new JLabel("Brand:");
		prBrandTF=new JTextField("");
		prModel=new JLabel("Model:");
		prModelTF=new JTextField("");
		prPrice=new JLabel("Price:");
		prPriceTF=new JTextField("");
		prQuantity=new JLabel("Quantity:");
		prQuantityTF=new JTextField("");
		addPr=new JButton("Add");
		updPr=new JButton("Update");

		
		cp1=new JPanel();
		cp1.setLayout(new BoxLayout(cp1, BoxLayout.PAGE_AXIS));
		cp1.add(prID);
		cp1.add(prIDTF);
		cp1.add(prBrand);
		cp1.add(prBrandTF);
		cp1.add(prModel);
		cp1.add(prModelTF);
		cp1.add(prPrice);
		cp1.add(prPriceTF);
		cp1.add(prQuantity);
		cp1.add(prQuantityTF);
		cp1.add(addPr); cp1.add(updPr);
		
		cp2=new JPanel();
		cp2.setLayout(new BoxLayout(cp2,BoxLayout.PAGE_AXIS));
		
		
		mainFr.add(cp1);
		mainFr.add(cp2);
		mainFr.setVisible(true);
		
		String[] col={"id","brand","model","price","quantity"};
		tProducts=new JTable(new DefaultTableModel(createDataM(products()),col));
		tProducts.setFillsViewportHeight(true);
		JScrollPane scrollPane=new JScrollPane(tProducts);
		scrollPane.setPreferredSize(new Dimension(700,150));
		cp2.add(scrollPane);
		
		
		
	}
	
	public void addAction(ActionListener addal) {
		addPr.addActionListener(addal);
	}
	
	public void updateAction(ActionListener updal) {
		updPr.addActionListener(updal);
	}
	
	
}
