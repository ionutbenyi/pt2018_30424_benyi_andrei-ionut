package Presentation;

import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.sql.*;
import com.mysql.cj.core.result.Field;

import ConnectionDB.ConnectionFactory;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.*;
import Model.Client;
import Model.Product;
import DAO.ClientDAO;
import DAO.ProductDAO;

public class Controller2 {
	
	private View2 v;
	
	public Controller2(View2 v2) {
		this.v=v2;
		
		v.addAction(new AddListener());
		v.updateAction(new UpdateListener());
	}
	
	
	class AddListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(v.prBrandTF.getText().isEmpty() || v.prModelTF.getText().isEmpty() || v.prPriceTF.getText().isEmpty() || v.prQuantityTF.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Error. Check parameters");
			}
			else {
				String prBrand=v.prBrandTF.getText();
				String prModel=v.prModelTF.getText();
				int prPrice=Integer.parseInt(v.prPriceTF.getText());
				int prQuantity=Integer.parseInt(v.prQuantityTF.getText());
				Product p=new Product(prBrand,prModel,prPrice,prQuantity);
				p=ProductDAO.insert(p);
				
				v.mainFr.setVisible(false);
				v=new View2();
			}
		}
	}
	
	class UpdateListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(v.prIDTF.getText().isEmpty() || v.prBrandTF.getText().isEmpty() || v.prModelTF.getText().isEmpty() || v.prPriceTF.getText().isEmpty() || v.prQuantityTF.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Error. Check parameters");
			}
			else {
				int prID=Integer.parseInt(v.prIDTF.getText());
				String prBrand=v.prBrandTF.getText();
				String prModel=v.prModelTF.getText();
				int prPrice=Integer.parseInt(v.prPriceTF.getText());
				int prQuantity=Integer.parseInt(v.prQuantityTF.getText());
				Product p=new Product(prBrand,prModel,prPrice,prQuantity);
				p=ProductDAO.update(p,prID);
				
				v.mainFr.setVisible(false);
				v=new View2();
			}
		}
	}
}
