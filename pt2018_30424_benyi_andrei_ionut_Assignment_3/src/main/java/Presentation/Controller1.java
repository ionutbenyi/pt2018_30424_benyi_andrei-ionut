package Presentation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import DAO.ClientDAO;
import DAO.ProductDAO;
import Model.Client;
import Model.Order1;
import Model.Product;
/**
 * The controller of the main GUI Window
 * @author Andrei Ionut Benyi
 *
 */
public class Controller1 {
	
	private View1 v;
	/**
	 * The constructor 
	 * @param v1 the controller takes as param the View1 type object
	 */
	public Controller1(View1 v1) {
		this.v=v1;
		v.clAction(new ClientListener());
		v.productAction(new ProductListener());
		v.buyAction(new BuyListener());
	}
	class ClientListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			View3 v3=new View3();
			Controller3 c3=new Controller3(v3);
		}
	}
	
	class ProductListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			View2 v2=new View2();
			Controller2 c2=new Controller2(v2);
		}
	}
	
	class BuyListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			View4 v4=new View4();
			int idp=Integer.parseInt(v.prIDTF.getText());
			String s=new String("");
			switch(idp%5) {
			case 0:{
				s="15:32, 25.04.2018";
				break;
			}
			case 1:{
				s="5:00, 27.04.2018";
				break;
			}
			case 2:{
				s="17:32, 25.04.2018";
				break;
			}
			case 3:{
				s="2:12, 21.02.2018";
				break;
			}
			case 4:{
				s="15:00, 3.04.2018";
				break;
			}
			default:{
				break;
			}
			}
			Order1 o = new Order1(s,idp);
			Product p = ProductDAO.findUsingID(idp);
			p.setQuantity(p.getQuantity()-1);
			Product p3=ProductDAO.update(p, idp);
			Client c=new Client(v.clNameTF.getText(),v.clPhoneTF.getText(),v.clAddressTF.getText(),Integer.parseInt(v.clAgeTF.getText()),v.clEmailTF.getText());
			Client c2=ClientDAO.insert(c);
			v4.ta.setText("		Order date: "+o.getDate()+"\n"+"		Name: "+v.clNameTF.getText()+"\n"+"		Address: "+v.clAddressTF.getText()+"\n"+
					"		Phone: "+v.clPhoneTF.getText()+"\n"+"		Email: "+v.clEmailTF.getText()+"\n"+"		Product ID: "+p.getId()+"\n"+
					"		Brand: "+p.getBrand()+"\n"+"		Model: "+p.getModel()+"\n"+"		Price: "+p.getPrice());
		}
	}
}
