package Presentation;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionListener;

import javax.swing.*;

public class View1 {
	
	private JLabel prBrand;
	public JTextField prBrandTF;
	private JLabel prID;
	public JTextField prIDTF;
	private JLabel prModel;
	public JTextField prModelTF;
	private JLabel clName;
	public JTextField clNameTF;
	private JLabel clPhone;
	public JTextField clPhoneTF;
	private JLabel clAddress;
	public JTextField clAddressTF;
	private JLabel clAge;
	public JTextField clAgeTF;
	private JLabel clEmail;
	public JTextField clEmailTF;
	
	private JButton buyButton;
	private JButton showProducts;
	private JButton showClients;
	
	private JPanel cp1;
	private JPanel cp2;
	private JPanel cp3;
	private JPanel cp4;
	private JPanel cp5;
	private JPanel cp6;
	private JPanel cp7;
	private JPanel cp8;
	private JPanel cp9;
	
	
	private JFrame mainFr=new JFrame();
	public View1() {
		mainFr.setSize(300,300);
		mainFr.setLocationRelativeTo(null);
		mainFr.setLayout(new FlowLayout());
		prBrand=new JLabel("Product brand:");
		prBrandTF=new JTextField("");
		prID=new JLabel("Product ID:");
		prIDTF=new JTextField("");
		prModel=new JLabel("Product model:");
		prModelTF=new JTextField("");
		clName=new JLabel("Client name:");
		clNameTF=new JTextField("");
		clPhone=new JLabel("Client phone:");
		clPhoneTF=new JTextField("");
		clAddress=new JLabel("Client address:");
		clAddressTF=new JTextField("");
		clAge=new JLabel("Client age:");
		clAgeTF=new JTextField("");
		clEmail=new JLabel("Client email:");
		clEmailTF=new JTextField("");
		
		buyButton=new JButton("Buy");
		showProducts=new JButton("Products");
		showClients=new JButton("Clients");
		
		cp1=new JPanel();
		cp1.setLayout(new BoxLayout(cp1,BoxLayout.PAGE_AXIS));
		cp1.add(prBrand);
		cp1.add(prBrandTF);
		
		cp2=new JPanel();
		cp2.setLayout(new BoxLayout(cp2,BoxLayout.PAGE_AXIS));
		cp2.add(prModel);
		cp2.add(prModelTF);
		
		cp3=new JPanel();
		cp3.setLayout(new BoxLayout(cp3,BoxLayout.PAGE_AXIS));
		cp3.add(clName);
		cp3.add(clNameTF);
		
		cp4=new JPanel();
		cp4.setLayout(new BoxLayout(cp4,BoxLayout.PAGE_AXIS));
		cp4.add(clPhone);
		cp4.add(clPhoneTF);
		
		cp5=new JPanel();
		cp5.setLayout(new BoxLayout(cp5,BoxLayout.PAGE_AXIS));
		cp5.add(clAddress);
		cp5.add(clAddressTF);
		
		cp6=new JPanel();
		cp6.setLayout(new BoxLayout(cp6,BoxLayout.PAGE_AXIS));
		cp6.add(clAge);
		cp6.add(clAgeTF);
		
		cp7=new JPanel();
		cp7.setLayout(new BoxLayout(cp7,BoxLayout.PAGE_AXIS));
		cp7.add(clEmail);
		cp7.add(clEmailTF);
		
		cp9=new JPanel();
		cp9.setLayout(new BoxLayout(cp9,BoxLayout.PAGE_AXIS));
		cp9.add(prID);
		cp9.add(prIDTF);
		
		cp8=new JPanel();
		cp8.setLayout(new BoxLayout(cp8,BoxLayout.PAGE_AXIS));
		cp8.add(buyButton);
		cp8.add(showProducts);
		cp8.add(showClients);
		
		
		mainFr.add(cp1);
		mainFr.add(cp2);
		mainFr.add(cp3);
		mainFr.add(cp4);
		mainFr.add(cp5);
		mainFr.add(cp6);
		mainFr.add(cp7);
		mainFr.add(cp9);
		mainFr.add(cp8);
		
		mainFr.setVisible(true);
	}
	
	public void buyAction(ActionListener bal) {
		buyButton.addActionListener(bal);
	}
	public void productAction(ActionListener pal) {
		showProducts.addActionListener(pal);
	}
	public void clAction(ActionListener cal) {
		showClients.addActionListener(cal);
	}
	
}
