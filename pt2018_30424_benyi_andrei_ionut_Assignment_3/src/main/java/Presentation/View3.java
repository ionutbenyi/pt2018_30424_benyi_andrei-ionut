package Presentation;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import ConnectionDB.ConnectionFactory;
import Model.Client;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.*;

public class View3 {
	
	
	public JFrame mainFr=new JFrame();
	private JPanel mainPanel=new JPanel();
	
	public JTable tClients=new JTable();
	public JScrollPane a=new JScrollPane(tClients);

	private JLabel clID;
	public JTextField clIDTF;
	private JLabel clName;
	public JTextField clNameTF;
	private JLabel clPhone;
	public JTextField clPhoneTF;
	private JLabel clAddress;
	public JTextField clAddressTF;
	private JLabel clAge;
	public JTextField clAgeTF;
	private JLabel clEmail;
	public JTextField clEmailTF;
	
	private JButton addCl;
	private JButton updCl;
	
	public JPanel cp1;
	public JPanel cp2;
	
	//-----------------------------------------------------
	public ArrayList<Client> clients(){
		ArrayList<Client> ac=new ArrayList<Client>();
		
		try {
			Connection dbConnection = ConnectionFactory.getConnection();
			String inst="SELECT * FROM Client";
			Statement stat=dbConnection.createStatement();
			ResultSet resSet=stat.executeQuery(inst);
			
			Client c;
			while(resSet.next()) {
				c=new Client(resSet.getInt("id"), resSet.getString("name"), resSet.getString("phone"), resSet.getString("address"), resSet.getInt("age"), resSet.getString("email"));
				ac.add(c);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return ac;
	}
	
	public static String[][] createDataM(ArrayList c ){
		String [][]res=new String[c.size()][c.get(0).getClass().getDeclaredFields().length];
		
		int i=0,j=0;
		for(Object cl: c) {
			j=0;
			for(java.lang.reflect.Field f: cl.getClass().getDeclaredFields()) {
				f.setAccessible(true);
				try {
					res[i][j]=f.get(cl).toString();
				}catch(IllegalArgumentException e) {
					e.printStackTrace();
				}catch(IllegalAccessException e) {
					e.printStackTrace();
				}
				j++;
			}
			i++;
		}
		
		return res;
	}
	//-----------------------------------------------------
	
	public View3() {
		mainFr.setSize(1000,300);
		mainFr.setLocationRelativeTo(null);
		mainFr.setLayout(new FlowLayout());
		clID=new JLabel("Client ID:");
		clIDTF=new JTextField("");
		clName=new JLabel("Client name:");
		clNameTF=new JTextField("");
		clPhone=new JLabel("Client phone:");
		clPhoneTF=new JTextField("");
		clAddress=new JLabel("Client address:");
		clAddressTF=new JTextField("");
		clAge=new JLabel("Client age:");
		clAgeTF=new JTextField("");
		clEmail=new JLabel("Client email:");
		clEmailTF=new JTextField("");
		addCl=new JButton("Add");
		updCl=new JButton("Update");
			
			cp1=new JPanel();
		cp1.setLayout(new BoxLayout(cp1, BoxLayout.PAGE_AXIS));
		cp1.add(clID);
		cp1.add(clIDTF);
		cp1.add(clName);
		cp1.add(clNameTF);
		cp1.add(clPhone);
		cp1.add(clPhoneTF);
		cp1.add(clAddress);
		cp1.add(clAddressTF);
		cp1.add(clAge);
		cp1.add(clAgeTF);
		cp1.add(clEmail);
		cp1.add(clEmailTF);
		cp1.add(addCl);
		
		cp2=new JPanel();
		cp2.setLayout(new BoxLayout(cp2,BoxLayout.PAGE_AXIS));
		
		
		mainFr.add(cp1);
		mainFr.add(cp2);
		mainFr.setVisible(true);
		
		String[] col={"id","name","phone","address","age","email"};
		tClients=new JTable(new DefaultTableModel(createDataM(clients()),col));
		tClients.setFillsViewportHeight(true);
		JScrollPane scrollPane=new JScrollPane(tClients);
		scrollPane.setPreferredSize(new Dimension(700,150));
		cp2.add(scrollPane);
		
		
	}
	
	public void addAction(ActionListener addal) {
		addCl.addActionListener(addal);
	}
	
	public void updateAction(ActionListener updal) {
		addCl.addActionListener(updal);
	}
	
}
