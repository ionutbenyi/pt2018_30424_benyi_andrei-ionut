package ConnectionDB;

import java.util.logging.*;
import java.util.*;
import java.sql.*;

public class ConnectionFactory {

	private static final Logger LOGGER=Logger.getLogger(ConnectionFactory.class.getName());
	private static final String DRIVER="com.mysql.cj.jdbc.Driver";
	private static final String DBURL="jdbc:mysql://localhost:3306/warehouse";
	private static final String USER="root";
	private static final String PASS="root";
	
	private static ConnectionFactory singleInstance=new ConnectionFactory();
	
	private ConnectionFactory() {
		try {
			Class.forName(DRIVER);
		}
		catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private Connection createConnection() {
		Connection con=null;
		try {
			con=DriverManager.getConnection(DBURL, USER, PASS);
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		
		return con;
	}
	
	public static Connection getConnection() {
		return singleInstance.createConnection();
	}
	
	public static void close(Connection con) {
		if(con!=null) {
			try {
				con.close();
			}
			catch(SQLException e) {
				LOGGER.log(Level.WARNING,"Error while closing");
			}
		}
	}
	
	public static void close(Statement st) {
		if(st!=null) {
			try {
				st.close();
			}
			catch(SQLException e) {
				LOGGER.log(Level.WARNING,"Error while closing");
			}
		}
	}
	
	public static void close(ResultSet rs) {
		if(rs!=null) {
			try {
				rs.close();
			}
			catch(SQLException e) {
				LOGGER.log(Level.WARNING,"Error while closing");
			}
		}
	}
}
