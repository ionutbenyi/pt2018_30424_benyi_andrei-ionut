package DAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import ConnectionDB.ConnectionFactory;
import Model.Order1;
import Model.Product;
/**
 * he class for handling the SQL operations on the database
 * @author Andrei Ionut Benyi
 *
 */
public class ProductDAO {
	
	protected static final Logger LOGGER= Logger.getLogger(ProductDAO.class.getName());
	private static final String insertStatement= "INSERT into Product (brand,model,price,quantity)" + "VALUES (?,?,?,?)";
	private static final String selectStatement = "SELECT * from Product where id=?";
	private static final String updateStatement = "UPDATE Product SET brand=?, model=?, price=?, quantity=?"+"WHERE id=?";
	
	/**
	 * The method for SELECT * where id=?
	 * @param idF the specific position
	 * @return
	 */
	public static Product findUsingID(int idF) {
		Product p=null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement selectStat = null;
		ResultSet resSet=null;
		try {
			selectStat = dbConnection.prepareStatement(selectStatement);
			selectStat.setLong(1, idF);
			resSet=selectStat.executeQuery();
			resSet.next();
			
			String brand = resSet.getString("brand");
			String model = resSet.getString("model");
			int price = resSet.getInt("price");
			int quantity=resSet.getInt("quantity");
			
			p=new Product(idF,brand,model,price,quantity);
		}
		catch(SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:findUsingId" + e.getMessage());
		}
		finally {
			ConnectionFactory.close(resSet);
			ConnectionFactory.close(selectStat);
			ConnectionFactory.close(dbConnection);
		}
		return p;
	}
	/**
	 * The function corresponding to the INSERT query in SQL
	 * @param p	The Product to be inserted
	 * @return
	 */
	public static Product insert(Product p) {
		Connection dbConnection =null;
		PreparedStatement insertStat = null;
		String instr="INSERT INTO Product(id,brand,model,price,quantity) VALUES("+p.getId()+", '"+p.getBrand()+"', '"+p.getModel()+"', "+p.getPrice()+", "+p.getQuantity()+");";
		
		int success=0;
		try {
			dbConnection=ConnectionFactory.getConnection();
			insertStat=dbConnection.prepareStatement(instr);
			success=insertStat.executeUpdate();
		}
		catch(SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO: insert "+e.getMessage());
		}
		finally {
			ConnectionFactory.close(insertStat);
			ConnectionFactory.close(dbConnection);
		}
		if(success==1)
			return p;
		return null;
	}
	/**
	 * 
	 * @param p The Product to be introduced at a specific position
	 * @param idF The position of the product
	 * @return
	 */
	public static Product update(Product p,int idF) {
		Connection dbConnection =null;
		PreparedStatement updateStat = null;
		String instr="UPDATE Product SET brand='"+p.getBrand()+"', model='"+p.getModel()+"', price="+p.getPrice()+", quantity="+p.getQuantity()+" Where id=?";
	
		int success=0;
		try {
			
			dbConnection=ConnectionFactory.getConnection();
			updateStat=dbConnection.prepareStatement(instr);
			updateStat.setLong(1, idF);
			success=updateStat.executeUpdate();
		}
		catch(SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO: update "+e.getMessage());
		}
		finally {
			ConnectionFactory.close(updateStat);
			ConnectionFactory.close(dbConnection);
		}
		if(success==1)
			return p;
		return null;
	}
}
