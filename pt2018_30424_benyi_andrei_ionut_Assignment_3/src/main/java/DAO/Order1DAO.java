package DAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import ConnectionDB.ConnectionFactory;
import Model.Order1;
import Model.Product;
/**
 * The class for handling the SQL operations on the database 
 * @author Andrei Ionut Benyi
 *
 */
public class Order1DAO {
	
	protected static final Logger LOGGER= Logger.getLogger(ProductDAO.class.getName());
	private static final String insertStatement= "INSERT into Order1 (date,prID)" + "VALUES (?,?)";
	private static final String selectStatement = "SELECT * from Order1 where id=?";
	private static final String updateStatement = "UPDATE Order1 SET date=?"+"WHERE id=?";
	/**
	 * The method for SELECT * where id=?
	 * @param idF The id representing which line is selected
	 * @return
	 */
	public static Order1 findUsingID(int idF) {
		Order1 o=null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement selectStat = null;
		ResultSet resSet=null;
		try {
			selectStat = dbConnection.prepareStatement(selectStatement);
			selectStat.setInt(1, idF);
			resSet=selectStat.executeQuery();
			resSet.next();
			
			String date = resSet.getString("date");
			int prID=resSet.getInt("prID");
			
			o=new Order1(idF,date,prID);
		}
		catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Order1DAO:findUsingId" + e.getMessage());
		}
		finally {
			ConnectionFactory.close(resSet);
			ConnectionFactory.close(selectStat);
			ConnectionFactory.close(dbConnection);
		}
		return o;
	}
	/**
	 * The equivalent of INSERT SQL query
	 * @param o	The Order to be inserted
	 * @return
	 */
	public static Order1 insert(Order1 o) {
		Connection dbConnection =null;
		PreparedStatement insertStat = null;
		String instr="INSERT INTO Order1(id,date,prID) VALUES("+o.getId()+", '"+o.getDate()+"', "+o.getPrID()+");";
		
		int success=0;
		try {
			dbConnection=ConnectionFactory.getConnection();
			insertStat=dbConnection.prepareStatement(instr);
			success=insertStat.executeUpdate();
		}
		catch(SQLException e) {
			LOGGER.log(Level.WARNING, "Order1DAO: insert "+e.getMessage());
		}
		finally {
			ConnectionFactory.close(insertStat);
			ConnectionFactory.close(dbConnection);
		}
		if(success==1)
			return o;
		return null;
	}
	/**
	 * 
	 * @param p	The Order to be introduced at a specific position
	 * @param idF	the specific position
	 * @return
	 */
	public static Order1 update(Order1 p,int idF) {
		Connection dbConnection =null;
		PreparedStatement updateStat = null;
		String instr="UPDATE Order1 SET date='"+p.getDate()+"', prID="+p.getPrID()+" Where id=?";
	
		int success=0;
		try {
			
			dbConnection=ConnectionFactory.getConnection();
			updateStat=dbConnection.prepareStatement(instr);
			updateStat.setLong(1, idF);
			success=updateStat.executeUpdate();
		}
		catch(SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO: update "+e.getMessage());
		}
		finally {
			ConnectionFactory.close(updateStat);
			ConnectionFactory.close(dbConnection);
		}
		if(success==1)
			return p;
		return null;
	}
}
