package DAO;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mysql.cj.api.mysqla.result.Resultset.Type;

import ConnectionDB.ConnectionFactory;
import Model.Client;
import Model.Product;
/**
 * The class for handling the SQL operations on the database
 * @author Andrei Ionut Benyi
 *
 */

 public class ClientDAO {
	
	protected static final Logger LOGGER= Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatement= "INSERT into Client (name,phone,address,age,email)" + "VALUES (?,?,?,?,?)";
	private static final String selectStatement = "SELECT * from Client where id=?";
	private static final String updateStatement = "UPDATE Client SET name=?, phone=?, address=?, age=?, email=?"+"WHERE id=?";
	/**
	 * The method for SELECT * where id=?
	 * @param idF The id representing which line is selected
	 * @return
	 */
	public static Client findUsingID(int idF) {
		Client c=null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement selectStat = null;
		ResultSet resSet=null;
		try {
			selectStat = dbConnection.prepareStatement(selectStatement);
			selectStat.setLong(1, idF);
			resSet=selectStat.executeQuery();
			resSet.next();
			
			String name = resSet.getString("name");
			String phone = resSet.getString("phone");
			String address = resSet.getString("address");
			int age = resSet.getInt("age");
			String email = resSet.getString("email");
			
			c=new Client(idF,name,phone,address,age,email);
		}
		catch(SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:findUsingId" + e.getMessage());
		}
		finally {
			ConnectionFactory.close(resSet);
			ConnectionFactory.close(selectStat);
			ConnectionFactory.close(dbConnection);
		}
		return c;
	}
	/**
	 * The function corresponding to the INSERT query in SQL
	 * @param c	The new client to be inserted in the database
	 * @return
	 */
	public static Client insert(Client c) {
		Connection dbConnection = null;
		PreparedStatement insertStat = null;
		String instr="INSERT INTO Client (id,name, phone, address, age, email) "+"VALUES("+ c.getId()+", '"+c.getName()+"', '"
				+c.getPhone()+"', '"+c.getAddress()+"', '"+c.getAge()+"', '"+c.getEmail()+"');";
		int success=0;
		
		try {
			dbConnection=ConnectionFactory.getConnection();
			insertStat=dbConnection.prepareStatement(instr);
			success=insertStat.executeUpdate();
		}
		catch(SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO: insert "+e.getMessage());
		}
		finally {
			ConnectionFactory.close(insertStat);
			ConnectionFactory.close(dbConnection);
		}
		if(success==1) {
			return c;
		}
		return null;
	}
	/**
	 * 
	 * @param p	The Client to be introduced at a specific position
	 * @param idF The position of the client
	 * @return
	 */
	public static Client update(Client p,int idF) {
		Connection dbConnection =null;
		PreparedStatement updateStat = null;
		String instr="UPDATE Client SET name='"+p.getName()+"', phone='"+p.getPhone()+"', address='"+p.getAddress()+"', age="+p.getAge()+", email='"+p.getEmail()+"' "+" Where id=?";
	
		int success=0;
		try {
			
			dbConnection=ConnectionFactory.getConnection();
			updateStat=dbConnection.prepareStatement(instr);
			updateStat.setLong(1, idF);
			success=updateStat.executeUpdate();
		}
		catch(SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO: update "+e.getMessage());
		}
		finally {
			ConnectionFactory.close(updateStat);
			ConnectionFactory.close(dbConnection);
		}
		if(success==1)
			return p;
		return null;
	}
}
