package Assignment_2.Threads_processing;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;

public class Controller {
	
	private View v;
	private Scheduler sched;
	
	public Controller(View v1) {
		this.v=v1;
		
		v.runAction(new RunListener());
		v.analysisAction(new AnalysisListener());
	}
	
	class RunListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if((v.arrMinTF.getText().isEmpty()) || (v.arrMaxTF.getText().isEmpty()) || (v.prMinTF.getText().isEmpty()) || (v.prMaxTF.getText().isEmpty()) || (v.queuesNrTF.getText().isEmpty()) || (v.tasksNrTF.getText().isEmpty()) || (v.runTimeTF.getText().isEmpty())) {
				JOptionPane.showMessageDialog(null, "Error. Check the parameters again");
			}
			else {
				int minArrivalTime=Integer.valueOf(v.getArrMin());
				int maxArrivalTime=Integer.valueOf(v.getArrMax());
				int minProcessTime=Integer.valueOf(v.getPrMin());
				int maxProcessTime=Integer.valueOf(v.getPrMax());
				int nrTasks=Integer.valueOf(v.getTasksNr());
				int nrServers=Integer.valueOf(v.getQueuesNr());
				int simulationTime=Integer.valueOf(v.getRunTime());
			
				sched=new Scheduler(minArrivalTime,maxArrivalTime,minProcessTime,maxProcessTime,simulationTime,nrTasks,nrServers,v);
				sched.start();
			
				Timer t=new Timer();
				t.schedule(new TimerTask() {
					public void run() {					
						v.setActualArea(sched.printServers());
					}
				},0L,1000L);
			}
			
		}
	}
	
	class AnalysisListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String aProcT="Average processing time: "+String.format("%.01f", sched.avgProcessT())+"\n";
			//String peakHr=new String("N/a"+"\n");
				String peakHr="Peak hour: "+String.format("%d", sched.peakHour())+"\n";
			String avgWaitT="Average waiting time: "+String.format("%.01f", sched.getAvgWaitT())+"\n";
			String fin=aProcT+avgWaitT+peakHr;
			JOptionPane.showMessageDialog(null, fin);
		}
	}
}