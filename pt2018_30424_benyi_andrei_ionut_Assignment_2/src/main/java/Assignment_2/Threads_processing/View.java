package Assignment_2.Threads_processing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class View {
	
	private JLabel arrMinTimeLabel;
	public JTextField arrMinTF;
	private JLabel arrMaxTimeLabel;
	public JTextField arrMaxTF;
	private JLabel prMinTimeLabel;
	public JTextField prMinTF;
	private JLabel prMaxTimeLabel;
	public JTextField prMaxTF;
	private JLabel queuesNrLabel;
	public JTextField queuesNrTF;
	private JLabel tasksNrLabel;
	public JTextField tasksNrTF;
	private JLabel runTimeLabel;
	public JTextField runTimeTF;
	
	private JPanel cp1;
	private JPanel cp2;
	private JPanel cp3;
	private JPanel cp4;
	private JPanel cp5;
	private JPanel cp6;
	private JPanel cp7;
	private JPanel cp8;
	private JPanel cp9;
	private JPanel cp10;
	
	private JButton runButton;
	private JButton analysisButton;
	
	private JFrame mainFr=new JFrame();
	private JPanel mainPanel=new JPanel();
	private JScrollPane infoScroll;
	private JScrollPane actScroll;
	private static JTextArea infoArea=new JTextArea("");
	private static JTextArea actualArea=new JTextArea("");
	
	//setting the output format
	public static void setInfoArea(String s) {
		infoArea.setText(s);
	}
	
	public static void addInfoArea(String s) {
		infoArea.append(s+"\n");
	}
	
	public static void setActualArea(String s) {
		actualArea.setText(s);
	}
	
	//getting the text field values to process
	public String getArrMin() {
		return this.arrMinTF.getText();
	}
	
	public String getArrMax() {
		return this.arrMinTF.getText();
	}
	
	public String getPrMin() {
		return this.prMinTF.getText();
	}
	
	public String getPrMax() {
		return this.prMaxTF.getText();
	}
	
	public String getQueuesNr() {
		return this.queuesNrTF.getText();
	}
	
	public String getTasksNr() {
		return this.tasksNrTF.getText();
	}
	
	public String getRunTime() {
		return this.runTimeTF.getText();
	}
	
	public View() {
		
		mainFr.setSize(500,450);
		//mainFr.setLayout(new GridLayout(4,2));
		mainFr.setLayout(new FlowLayout());
		arrMinTimeLabel=new JLabel("Arrival min time:");
		arrMaxTimeLabel=new JLabel("Arrival max time:");
		prMinTimeLabel=new JLabel("Processing min time:");
		prMaxTimeLabel=new JLabel("Processing max time:");
		queuesNrLabel=new JLabel("Nr of queues:");
		tasksNrLabel=new JLabel("Nr of clients:");
		runTimeLabel=new JLabel("Run time:");
		
		arrMinTF=new JTextField("");
		arrMaxTF=new JTextField("");
		prMinTF=new JTextField("");
		prMaxTF=new JTextField("");
		queuesNrTF=new JTextField("");
		tasksNrTF=new JTextField("");
		runTimeTF=new JTextField("");
		
		runButton=new JButton("RUN");
		analysisButton=new JButton("Analysis");
		
		cp1=new JPanel();
		//cp1.setLayout(new FlowLayout());
		cp1.setLayout(new BoxLayout(cp1,BoxLayout.PAGE_AXIS));
		cp1.add(arrMinTimeLabel);
		cp1.add(arrMinTF);
		
		cp2=new JPanel();
		cp2.setLayout(new BoxLayout(cp2,BoxLayout.PAGE_AXIS));
		cp2.add(arrMaxTimeLabel);
		cp2.add(arrMaxTF);
		
		cp3=new JPanel();
		cp3.setLayout(new BoxLayout(cp3,BoxLayout.PAGE_AXIS));
		cp3.add(prMinTimeLabel);
		cp3.add(prMinTF);
		
		cp4=new JPanel();
		cp4.setLayout(new BoxLayout(cp4,BoxLayout.PAGE_AXIS));
		cp4.add(prMaxTimeLabel);
		cp4.add(prMaxTF);
		
		cp5=new JPanel();
		cp5.setLayout(new BoxLayout(cp5,BoxLayout.PAGE_AXIS));
		cp5.add(queuesNrLabel);
		cp5.add(queuesNrTF);
		
		cp6=new JPanel();
		cp6.setLayout(new BoxLayout(cp6,BoxLayout.PAGE_AXIS));
		cp6.add(tasksNrLabel);
		cp6.add(tasksNrTF);
		
		cp7=new JPanel();
		cp7.setLayout(new BoxLayout(cp7,BoxLayout.PAGE_AXIS));
		cp7.add(runTimeLabel);
		cp7.add(runTimeTF);
		
		cp8=new JPanel();
		cp8.setLayout(new FlowLayout());
		cp8.add(runButton);
		cp8.add(analysisButton);
		
		cp9=new JPanel();
		cp9.setSize(100, 50);
		cp9.setLayout(new BoxLayout(cp9,BoxLayout.PAGE_AXIS));
		//infoArea.setSize(100, 50);
		infoScroll=new JScrollPane(infoArea);
		infoScroll.setPreferredSize(new Dimension(300,300));
		cp9.add(infoScroll);
		
		
		cp10=new JPanel();
		cp10.setSize(100, 50);
		cp10.setLayout(new BoxLayout(cp10,BoxLayout.PAGE_AXIS));
		actualArea.setSize(200, 80);
		actScroll=new JScrollPane(actualArea);
		actScroll.setPreferredSize(new Dimension(160,100));
		cp10.add(actScroll);
		
		mainFr.add(cp1);
		mainFr.add(cp2);
		mainFr.add(cp3);
		mainFr.add(cp4);
		mainFr.add(cp5);
		mainFr.add(cp6);
		mainFr.add(cp7);
		mainFr.add(cp8);
		mainFr.add(cp9);
		mainFr.add(cp10);
		
		mainFr.setVisible(true);
	}
	
	public void runAction(ActionListener al) {
		runButton.addActionListener(al);
	}
	
	public void analysisAction(ActionListener anl) {
		analysisButton.addActionListener(anl);
	}

	
}
