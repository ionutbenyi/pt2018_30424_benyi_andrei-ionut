package Assignment_2.Threads_processing;
import java.util.*;
public class Scheduler extends Thread {
	
	private int minArrTime;
	private int maxArrTime;
	private int minProcTime;
	private int maxProcTime;
	
	private static int nrTasks;
	private static int nrServers;
	private static int simT;
	
	private static int totalProcessT=0;
	private static int avgWaitT=0;
	private static int totalWaitingT=0;
	
	private static int crtT=0;	//time when the simulation will start
	Random r=new Random();
	
	private static int taskMax=0;	//peak hour calc parameters
	private static int peakH=0;
	
	
	private static ArrayList<Server> s=new ArrayList<Server>();	//as in a store, we have many Servers
	private static ArrayList<Task> tArray=new ArrayList<Task>(); 	//the tasks come here
	
	private View v;
	
	public static float avgProcessT() {		//avg Process time
		return totalProcessT/getNrTasks();
	}
	
	public static int peakHour() {				//peak hour
		int tasksTotal=0;
		for(int i=0;i<nrServers;i++) {
			tasksTotal=tasksTotal+s.get(i).getQSize();
		}
		if(tasksTotal>taskMax) {
			taskMax=tasksTotal;
			peakH=crtT;
		}
		System.out.print(tasksTotal+" ");
		return peakH;
	}
	
	public static float getAvgWaitT() {			//average wait time
		return totalWaitingT/getNrTasks();
	}
	
	static void addWaitingTime(int t) {
		totalWaitingT=totalWaitingT+t;
	}
	
	public static int getNrTasks() {
		return nrTasks;
	}
	
	public static int getNrServers() {
		return nrServers;
	}
	
	public static int getSimTime() {
		return simT;
	}
	
	public static int getCurrentTime() {
		return crtT;
	}
	
	public Scheduler(int minA,int maxA,int minP,int maxP,int sT,int nrT,int nrS,View view) {
		minArrTime=minA;
		maxArrTime=maxA;
		minProcTime=minP;
		maxProcTime=maxP;
		simT=sT;
		nrTasks=nrT;
		nrServers=nrS;
		v=view;
		
		for(int i=0;i<nrServers;i++) {
			s.add(new Server());
		}
	}
	
	public String printServers() {
		String st="";
		for(int i=0;i<nrServers;i++) {
			st=st+"Server "+(i+1)+": ";
			for(Task t1:s.get(i).getQ()) {
				st=st+"T["+(t1.getId())+"] ";
			}
			st=st+"\n";
		}
		return st;
	}
	
	public int getMinT() {
		int i=0;
		if(s.size()!=0) {
			int min=s.get(0).getQSize();
			for(int j=1;j<s.size();j++) {
				if(s.get(j).getQSize()<min) {
					min=s.get(j).getQSize();
					i=j;
				}
			}
		}
		return i;
	}
	
	public void run() {
		int a=0;
		//initialize the temporary array
		for(int i=0;i<nrTasks;i++) {
			int aT=a+r.nextInt(maxArrTime-minArrTime+1)+minArrTime;
			a=aT;
			int pT=r.nextInt(maxProcTime-minProcTime+1)+minProcTime;
			Task aux=new Task(i+1,aT,pT);
			tArray.add(aux);
		}
		
		for(Server s1:s) {
			s1.start();
		}
		
		for(crtT=1;crtT<=simT;crtT++) {
			
			v.addInfoArea("Time "+crtT+" ");
			for(Task t1:tArray) {
				if(t1.getStart()==crtT) {
				
					int smIndex=getMinT();
					s.get(smIndex).addTask(t1);
					totalProcessT=totalProcessT+t1.getProcessing();
				
					String st="Task["+ t1.getId()+"], at Server "+ smIndex+ ", ArrTime:"
						+ t1.getStart()+"ProcTime: "+t1.getProcessing();
					View.addInfoArea(st);
				}
			}
			try {
				Thread.sleep(1000);
			}
			catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}
