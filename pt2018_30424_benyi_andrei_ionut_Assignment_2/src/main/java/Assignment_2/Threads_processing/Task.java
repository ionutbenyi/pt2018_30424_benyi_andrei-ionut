package Assignment_2.Threads_processing;
import java.util.*;

public class Task {

	private int id;
	private int start;
	private int processing;
	
	public Task(int i, int s, int p) {
		this.id=i;
		this.start=s;
		this.processing=p;
	}
	
	public int getId() {
		return this.id;
	}
	
	public int getStart() {
		return this.start;
	}
	
	public int getProcessing() {
		return this.processing;
	}
	
}
