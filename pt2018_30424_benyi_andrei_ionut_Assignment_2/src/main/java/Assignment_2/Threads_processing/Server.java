package Assignment_2.Threads_processing;

import java.util.*;

public class Server extends Thread{

	LinkedList<Task> q=new LinkedList<Task>();
	private int qEmptyT=0;
	
	//get object's attributes
	public LinkedList<Task> getQ(){
		return q;
	}
	
	public int getEmptyT() {
		return qEmptyT;
	}
	
	public int getQSize() {
		return q.size();
	}
	
	//set object's attributes
	public void setQEmptyT(int t) {
		this.qEmptyT=t;
	}
	
	public void setQ(LinkedList<Task> q1) {
		this.q=q1;
	}
	
	//add and remove Tasks
	
	public void addTask(Task t) {
		this.q.add(t);
	}
	
	public void removeTask() {
		this.q.remove();
	}
	
	public void run() {
		while(Scheduler.getCurrentTime()<Scheduler.getSimTime()) {
			if(!(q.isEmpty())) {
				//if we have tasks in the server, we put the thread to sleep
				//sleep time=the processing time of the first task from the server
				try {
					Thread.sleep(q.getFirst().getProcessing()*1000);
					String st="Task "+q.getFirst().getId()+" left at "+Scheduler.getCurrentTime();
					View.addInfoArea(st);
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					//Task completed, so we remove it
					Scheduler.addWaitingTime(Scheduler.getCurrentTime()-q.getFirst().getStart()-q.getFirst().getProcessing());
					q.removeFirst();
				}
				
			}
			else {
				//increment the server empty time
				qEmptyT++;
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
