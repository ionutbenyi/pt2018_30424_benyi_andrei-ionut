package Assignment1.PolynomialsProject;

import static org.junit.Assert.*;

import org.junit.Test;

public class DerCase {

	@Test
	public void testDer() {
		PolOp test=new PolOp();
		Polynomial rez=new Polynomial(1);
		rez.addMon(new Monomial(1,0));
		rez.addMon(new Monomial(4,1));
		
		Polynomial p1=new Polynomial(2);
		p1.addMon(new Monomial(1,0));
		p1.addMon(new Monomial(1,1));
		p1.addMon(new Monomial(2,2));
	
		Polynomial pr=test.derPT(p1);
		
		for(int i=0;i<=1;i++) {
			assertEquals(pr.pol.get(i).coeff,rez.pol.get(i).coeff,0);
		}
	}

}
