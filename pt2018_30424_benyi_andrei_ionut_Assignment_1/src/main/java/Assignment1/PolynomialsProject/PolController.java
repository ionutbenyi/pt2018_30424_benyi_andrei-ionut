package Assignment1.PolynomialsProject;

import java.awt.event.*;
import java.util.*;
import java.math.*;
public class PolController {
	private PolModel m_model;
	private PolView m_view;
	int i=0;
	int j=0;
	PolController(PolModel model, PolView view){
		m_model=model;
		m_view=view;
		
		m_view.addSet1Listener(new Set1Listener());
		m_view.addSet2Listener(new Set2Listener());
		m_view.addAddListener(new AddListener());
		m_view.addSubListener(new SubListener());
		m_view.addMulListener(new MulListener());
		m_view.addDivListener(new DivListener());
		m_view.addDerListener(new DerListener());
		m_view.addIntListener(new IntListener());
		m_view.addResetListener(new ResetListener());
	}
	
	class Set1Listener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			int dg=Integer.parseInt(m_view.degTextf.getText());
			m_view.powerLabel.setText("Power "+(m_model.p1.pol.size()+1)+": ");
			int coef=Integer.parseInt(m_view.coeffTextf.getText());
			m_view.coeffTextf.setText("");
			if(i<=dg) {
			Monomial m=new Monomial(coef,i);
			m_model.p1.addMon(m);
			i++;
			String r=m_model.p1.printPol();
			m_view.inputPolTextf.setText(r);
			m_model.p1.degree=m_model.p1.pol.size()-1;
			}
			else {
				i=0;
				m_view.inputPolTextf.setText("Error");
				m_model.p1=new Polynomial(0);
			}
		}
	}
	
	class Set2Listener implements ActionListener {
		
		
		public void actionPerformed(ActionEvent e) {
			int dg2;
			dg2=Integer.parseInt(m_view.degTextf2.getText());
			m_view.powerLabel2.setText("Power "+(m_model.p2.pol.size()+1)+": ");
			int coef2=Integer.parseInt(m_view.coeffTextf2.getText());
			m_view.coeffTextf2.setText("");
			if(j<=dg2) {
			Monomial m2=new Monomial(coef2,j);
			m_model.p2.addMon(m2);
			j++;
			String r2=m_model.p2.printPol();
			m_view.inputPolTextf2.setText(r2);
			m_model.p2.degree=m_model.p2.pol.size()-1;	
			}
			else {
				j=0;
				m_view.inputPolTextf2.setText("Error");
				m_model.p2=new Polynomial(0);
			}
		}
	}
	
	class AddListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			m_view.outputPolTextf.setText(" ");
			m_model.rez=(m_model.p1).additionPol(m_model.p2);
			String t=m_model.rez.printPol();
			m_view.outputPolTextf.setText(t);
		}
	}
	
	class SubListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			m_view.outputPolTextf.setText(" ");
			m_model.rez=(m_model.p1).subtractionPol(m_model.p2);
			String t=m_model.rez.printPol();
			m_view.outputPolTextf.setText(t);
		}
	}
	
	class MulListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			m_view.outputPolTextf.setText(" ");
			m_model.rez=(m_model.p1).multiplicationPol(m_model.p2);
			String t=m_model.rez.printPol();
			m_view.outputPolTextf.setText(t);
		}
	} 
	
	class DivListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			m_view.outputPolTextf.setText(" ");
			m_model.rez=(m_model.p1).divisionPol(m_model.p2);
			String t=m_model.rez.printPol();
			m_view.outputPolTextf.setText(t);
		}
	}
	
	class DerListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			m_view.outputPolTextf.setText(" ");
			m_model.rez=(m_model.p1).differentiatePol();
			String t=m_model.rez.printPol();
			m_view.outputPolTextf.setText(t);
		}
	}
	
	class IntListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			m_view.outputPolTextf.setText(" ");
			m_model.rez=(m_model.p1).integratePol();
			String t=m_model.rez.printPol();
			m_view.outputPolTextf.setText(t);
		}
	}
	
	class ResetListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			m_view.outputPolTextf.setText("");
			m_view.inputPolTextf.setText("");
			m_view.inputPolTextf2.setText("");
			m_view.degTextf.setText("");
			m_view.degTextf2.setText("");
			m_view.coeffTextf.setText("");
			m_view.coeffTextf2.setText("");
			m_model.p1=new Polynomial(0);
			m_model.p2=new Polynomial(0);
			m_model.rez=new Polynomial(0);
			m_view.powerLabel.setText("Power 0:");
			m_view.powerLabel2.setText("Power 0:");
			i=0;
			j=0;
		}
	}
	
}
